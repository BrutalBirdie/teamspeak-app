This app packages Teamspeak <upstream>3.13.6</upstream>

### Overview

TeamSpeak is a proprietary voice-over-Internet Protocol (VoIP) application for audio communication between users on a chat channel, much like a telephone conference call. Users typically use headphones with a microphone. The client software connects to a TeamSpeak server of the user's choice, from which the user may join chat channels.

### License

By default this package uses free license, which includes a maximum of 32 concurrent users. Game or Commercial licenses can be obtained via the [TeamSpeak website](https://teamspeak.com/en/features/licensing/) and the app can then be configured to use the license key.
