#!/bin/bash

set -eu

mkdir -p /app/data/ts/files /run/teamspeak/logs

update_port() {
    set -eu

    sleep 10 # wait for table to appear
    echo "==> Updating teamspeak server port"
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "UPDATE servers SET server_port=${DEFAULT_PORT};"
}

# get/set ports
if [ -z ${DEFAULT_PORT+x} ]; then DEFAULT_PORT="8801"; else echo "DEFAULT_PORT is set to '$DEFAULT_PORT'"; fi
if [ -z ${SERVERQUERY_PORT+x} ]; then SERVERQUERY_PORT="10011"; else echo "SERVERQUERY_PORT is set to '$SERVERQUERY_PORT'"; fi
if [ -z ${FILETRANSFER_PORT+x} ]; then FILETRANSFER_PORT="30033"; else echo "FILETRANSFER_PORT is set to '$FILETRANSFER_PORT'"; fi

if [[ ! -f /app/data/ts/ts3server.ini ]]; then
    echo "=> Creating initial /app/data/ts/ts3server.ini"

    echo "
machine_id=
default_voice_port=${DEFAULT_PORT}
voice_ip=0.0.0.0
licensepath=/app/data/
filetransfer_port=${FILETRANSFER_PORT}
filetransfer_ip=0.0.0.0
query_port=${SERVERQUERY_PORT}
query_ip=0.0.0.0
dbplugin=ts3db_mariadb
dbpluginparameter=ts3db_mysql.ini
dbsqlpath=sql/
dbsqlcreatepath=create_mariadb/
logpath=/run/teamspeak/logs
logquerycommands=0
query_ip_allowlist=/app/data/ts/query_ip_allowlist.txt
query_ip_denylist=/app/data/ts/query_ip_denylist.txt
query_protocols=raw
query_ssh_rsa_host_key=ssh_host_rsa_key
" > /app/data/ts/ts3server.ini
fi

echo "
[config]
host='${CLOUDRON_MYSQL_HOST}'
port='${CLOUDRON_MYSQL_PORT}'
username='${CLOUDRON_MYSQL_USERNAME}'
password='${CLOUDRON_MYSQL_PASSWORD}'
database='${CLOUDRON_MYSQL_DATABASE}'
socket=
" > /run/teamspeak/ts3db_mysql.ini

# Ensure we have those files
[[ -f /app/data/ts/query_ip_whitelist.txt ]] && mv /app/data/ts/query_ip_whitelist.txt /app/data/ts/query_ip_allowlist.txt
[[ -f /app/data/ts/query_ip_blacklist.txt ]] && mv /app/data/ts/query_ip_blacklist.txt /app/data/ts/query_ip_denylist.txt
touch /app/data/ts/query_ip_allowlist.txt /app/data/ts/query_ip_denylist.txt

sed -e "s/default_voice_port=.*/default_voice_port=${DEFAULT_PORT}/" \
    -e "s/filetransfer_port=.*/filetransfer_port=${FILETRANSFER_PORT}/" \
    -e "s/query_port=.*/query_port=${SERVERQUERY_PORT}/" -i /app/data/ts/ts3server.ini

update_port &

echo "=> Ensuring permissions"
chown -R www-data.www-data /app/data /run/teamspeak

echo "=> Starting Teamspeak Server"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Teamspeak
